# Animated Cards

### Installation

```
npm install
```

### Run in development mode (isomorphic with hot module replacement)

```
npm run dev
```

### Make a production build

```
npm run build
```

### Google Login Setup

* Open https://console.developers.google.com/apis
  * Enable Google+ API
    * Credentials:
      * Note Client ID and Client Secret
      * Authorized JavaScript Origins: https://auth.firebase.com
      * Authorized Redirect URIs: https://auth.firebase.com/v2/<YOUR-FIREBASE-APP>/auth/google/callback

### Firebase Setup

* Login and Auth tab:
  * Authorized Domains: <YOUR-DOMAIN> (e.g. animated-cards.herokuapp.com)
  * Google: Fill Google Client ID and Google Client Secret
* Import fixtures/cards.json into Firebase
* Security and Rules should look something like:
```
{
  "rules": {
    ".read": false,
    ".write": false,
    "cards": {
      ".read": "auth != null"
    }
  }
}
```
* On app modify app/config.js with your firebaseUrl.
