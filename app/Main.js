import React, { Component, PropTypes } from 'react';
import Footer from './components/Footer';
import Nav from './components/Nav';
import { handleAuthByToken, handleWindowResize, resetErrorMessage } from './actions';
import { connect } from 'react-redux';
import authService from './services/auth';

class Main extends Component {
  constructor(props) {
    super(props);
    this.handleResize = this.handleResize.bind(this);
    this.handleDismissClick = this.handleDismissClick.bind(this);
  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.handleResize);

    const { authServiceToken, authToken, handleAuthByToken } = this.props;
    if (authToken !== authServiceToken) {
      handleAuthByToken(authServiceToken);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize() {
    const screenHeight = document.body.clientHeight;
    const screenWidth = document.body.clientWidth;
    const canvasSize = screenWidth < 400 ? screenWidth - 20 : 400;
    const windowInnerHeight = window.innerWidth;
    const windowInnerWidth = window.innerHeight;

    this.props.handleWindowResize({
      screenHeight,
      screenWidth,
      canvasSize,
      windowInnerHeight,
      windowInnerWidth,
    });
  }

  handleDismissClick(e) {
    this.props.resetErrorMessage();
    e.preventDefault();
  }

  renderErrorMessage() {
    const { errorMessage } = this.props;
    if (!errorMessage) {
      return null;
    }

    return (
      <p style={{ backgroundColor: '#e99', padding: 10 }}>
        <b>{errorMessage.message}</b>
        {' '}
        (<a href="#" onClick={this.handleDismissClick}>
          Dismiss
        </a>)
      </p>
    );
  }

  render() {
    return (
      <div className="GeneralLayout">
        <div className="pageWrap">
          <header className="HeaderContainer">
            <h1>ANIMATED CARDS</h1>
          </header>

          <Nav />

          {this.renderErrorMessage()}

          <section className="MainContainer">
            {this.props.children}
          </section>
        </div>

        <footer className="FooterContainer">
          <Footer />
        </footer>
      </div>
    );
  }
}

Main.propTypes = {
  children: PropTypes.node,
};

function mapStateToProps(state) {
  const { auth } = state;
  const authToken = auth.token;
  const authServiceToken = authService.getToken();

  return {
    authToken,
    authServiceToken,
    errorMessage: state.errorMessage,
  };
}

export default connect(mapStateToProps, {
  handleAuthByToken,
  handleWindowResize,
  resetErrorMessage,
})(Main);
