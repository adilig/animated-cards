import Firebase from 'firebase';
import { replace } from 'react-router-redux';
import authService from '../services/auth';
import { firebaseUrl } from '../config';

const ref = new Firebase(firebaseUrl);

function action(type, payload = {}) {
  return { type, ...payload };
}
const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILURE = 'FAILURE';

function createRequestTypes(base) {
  const res = {};
  [REQUEST, SUCCESS, FAILURE].forEach(type => {
    res[type] = `${base}_${type}`;
  });
  return res;
}

export const GOOGLE_LOGIN = createRequestTypes('GOOGLE_LOGIN');

const googleLogin = {
  request: () => action(GOOGLE_LOGIN.REQUEST),
  failure: (error) => action(GOOGLE_LOGIN.FAILURE, { error }),
  success: (authData) => action(GOOGLE_LOGIN.SUCCESS, { authData }),
};

export function handleGoogleLogin() {
  return dispatch => {
    dispatch(googleLogin.request());

    ref.authWithOAuthPopup('google', (error, authData) => {
      if (error) {
        dispatch(googleLogin.failure(error));
        return;
      }

      authService.login(authData.token, () => {
        dispatch(googleLogin.success(authData));
        dispatch(replace('/'));
      });
    });
  };
}

export const AUTH_BY_TOKEN = createRequestTypes('AUTH_BY_TOKEN');
const authByToken = {
  request: () => action(AUTH_BY_TOKEN.REQUEST),
  failure: (error) => action(AUTH_BY_TOKEN.FAILURE, { error }),
  success: (authData) => action(AUTH_BY_TOKEN.SUCCESS, { authData }),
};

export const handleAuthByToken = (authToken) => dispatch => {
  dispatch(authByToken.request());

  ref.authWithCustomToken(authToken, (error, authData) => {
    if (error) {
      dispatch(authByToken.failure(error));
      dispatch(replace('/login'));
      return;
    }
    dispatch(authByToken.success(authData));
  });
};


export const LOGOUT = 'LOGOUT';

const logout = () => action(LOGOUT);

export function handleLogout() {
  return dispatch => {
    dispatch(logout());
    authService.logout();
    dispatch(replace('/login'));
  };
}


export const HANDLE_WINDOW_RESIZE = 'HANDLE_WINDOW_RESIZE';
export function handleWindowResize(windowDimensions) {
  return dispatch => {
    dispatch(action(HANDLE_WINDOW_RESIZE, { windowDimensions }));
  };
}


export const CARDS_DATA = createRequestTypes('CARDS_DATA');

const cardsData = {
  request: () => action(CARDS_DATA.REQUEST),
  failure: (error) => action(CARDS_DATA.FAILURE, { error }),
  success: (data) => action(CARDS_DATA.SUCCESS, { data, receivedAt: Date.now() }),
};

export function fetchCardsData() {
  return dispatch => {
    dispatch(cardsData.request());

    ref.child('cards')
      .once('value')
      .then(snapshot => {
        const cards = snapshot.val();
        dispatch(cardsData.success(cards));
      })
      .catch(error => { dispatch(cardsData.failure(error)); });
  };
}


export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE';

// Resets the currently visible error message.
export function resetErrorMessage() {
  return {
    type: RESET_ERROR_MESSAGE,
  };
}

