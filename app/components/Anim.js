import React, { Component, PropTypes } from 'react';

const isBrowser = typeof(window) !== 'undefined';


export default class Anim extends Component {
  constructor(props) {
    super(props);

    this.state = {
      context: null,
    };
    if (isBrowser) {
      this.sun = new Image();
      this.moon = new Image();
      this.earth = new Image();
      this.sun.src = '/images/Canvas_sun.png';
      this.moon.src = '/images/Canvas_moon.png';
      this.earth.src = '/images/Canvas_earth.png';
    }
    this.requestId = null;
  }

  componentDidMount() {
    if (!isBrowser) { return; }

    const context = this.refs.canvas.getContext('2d');
    this.setState({ context });

    this.requestId = requestAnimationFrame(() => {this.update();});
    this.ticks = 0;
  }

  componentDidUpdate(prevProps) {
    if (!isBrowser) { return; }

    if (prevProps.isActive !== this.props.isActive) {
      if (this.props.isActive) {
        this.requestId = requestAnimationFrame(() => {this.update();});
      } else {
        // console.log('STOP', this.props.title, this.requestId);
        cancelAnimationFrame(this.requestId);
      }
    }
  }

  update() {
    const { height, width } = this.props;

    const ctx = this.state.context;

    ctx.globalCompositeOperation = 'destination-over';
    ctx.clearRect(0, 0, width, height); // clear canvas

    ctx.fillStyle = 'rgba(0,0,0,0.4)';
    ctx.strokeStyle = 'rgba(0,153,255,0.4)';
    ctx.save();
    ctx.translate(width / 2, height / 2);

    // Earth
    ctx.rotate(((2 * Math.PI) / 60000) * this.ticks);
    ctx.translate(105, 0);
    ctx.fillRect(0, -12, 50, 24); // Shadow
    ctx.drawImage(this.earth, -12, -12);

    // Moon
    ctx.save();
    ctx.rotate(((2 * Math.PI) / 6000) * this.ticks);
    ctx.translate(0, 28.5);
    ctx.drawImage(this.moon, -3.5, -3.5);
    ctx.restore();

    ctx.restore();

    ctx.beginPath();
    ctx.arc(width / 2, height / 2, 105, 0, Math.PI * 2, false);  // Earth orbit
    ctx.stroke();

    ctx.drawImage(this.sun, 0, 0, width, height);

    if (this.props.isActive) {
      this.ticks = this.ticks + 100;
    }

    this.requestId = requestAnimationFrame(() => {this.update();});
  // }
  }

  render() {
    const { height, width } = this.props;
    return (
      <canvas ref="canvas" width={width} height={height} />
    );
  }
}
Anim.propTypes = {
  isActive: PropTypes.bool,
  title: PropTypes.string,
  height: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
};

export default Anim;
