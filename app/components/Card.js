import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import colors from '../constants/colors';
import Anim from '../components/Anim';

const styles = {
  title: {
    backgroundColor: colors.CardTitleBg,
    padding: '10px',
    paddingTop: '35px',
  },
  contentWrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    margin: '10px',
  },
  content: {
    flex: 1,
    textAlign: 'center',
  },
};

class Card extends Component {
  constructor(props) {
    super(props);
    this.makeCardRef = this.makeCardRef.bind(this);
    this.state = {
      isCardActive: false,
    };
  }
  componentDidMount() {
    this.cardDOM = ReactDOM.findDOMNode(this.cardRef);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.bodyScrollTop !== this.props.bodyScrollTop) {
      const { windowDimensions: { canvasSize, screenHeight } } = this.props;
      this.cardDOM = ReactDOM.findDOMNode(this.cardRef);
      const top = this.cardDOM ? this.cardDOM.getBoundingClientRect().top : 9999;
      const isCardActive = top > -50 && top < (screenHeight - canvasSize) / 2;
      this.setState({
        isCardActive,
      });
    }
  }

  makeCardRef(cardRef) {
    this.cardRef = cardRef;
  }

  render() {
    const { isCardActive } = this.state;
    const { data, windowDimensions: {
      canvasSize, windowInnerHeight, windowInnerWidth,
    } } = this.props;

    const activeStyle = isCardActive ? { backgroundColor: colors.CardContentBg2 } : {};
    const contentWrapStyle = Object.assign(
      {},
      styles.contentWrap,
      { height: `${canvasSize}px` },
      activeStyle
    );
    return (
      <div ref={this.makeCardRef} className={'Card'}>
        <div style={styles.title}>{data.title}</div>

        <div style={contentWrapStyle}>
          <div style={styles.content}>
            <Anim
              isActive={isCardActive}
              title={data.title}
              height={canvasSize || 0}
              width={canvasSize || 0}
              windowInnerHeight={windowInnerHeight}
              windowInnerWidth={windowInnerWidth}
            />
          </div>
        </div>
      </div>
    );
  }
}

Card.propTypes = {
  bodyScrollTop: PropTypes.number,
  data: PropTypes.shape().isRequired,
};

export default Card;
