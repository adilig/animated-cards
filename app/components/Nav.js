import React, { Component } from 'react';
import { Link } from 'react-router';
import authService from '../services/auth';
import { connect } from 'react-redux';
const isBrowser = typeof(window) !== 'undefined';

const styles = {
  nav: {
    backgroundColor: 'black',
    padding: '5px',
  },
  link: {
    color: 'white',
    padding: '10px',
  },
};

class Nav extends Component {
  render() {
    const { isLoggedIn } = this.props;
    return (
      <nav style={styles.nav}>
        {isBrowser && isLoggedIn && (
          <div>
            <Link to="/" style={styles.link}>Home</Link>
            <Link to="/dashboard" style={styles.link}>Dashboard</Link>
            <Link to="/logout" style={styles.link}>Logout</Link>
          </div>
        )}
        {isBrowser && !isLoggedIn && (
          <div>
            <Link to="/login" style={styles.link}>Login</Link>
          </div>
        )}
      </nav>
    );
  }
}

function mapStateToProps(state) {
  const isLoggedIn = authService.loggedIn();
  return {
    isLoggedIn,
  };
}

export default connect(mapStateToProps)(Nav);
