const colors = {
  White: '#ffffff',
  LightGray: '#efefef',
  Dark: '#353944',
  Pink: '#f4cccc',
  Green: '#b6d7a8',
};

export default {
  CardTitleBg: colors.LightGray,
  CardContentBg1: colors.Pink,
  CardContentBg2: colors.Green,
};
