import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from '../components/Card';
import { fetchCardsData } from '../actions';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.renderCard = this.renderCard.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.cardRefs = [];
    this.state = {};
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    this.props.fetchCardsData();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    this.setState({ scrollTop: event.srcElement.body.scrollTop });
  }

  renderCard(card, i) {
    const { windowDimensions } = this.props;
    const { scrollTop } = this.state;
    const makeCardRef = (ref) => {
      this.cardRefs[i] = ref;
    };
    return (
      <Card
        ref={makeCardRef}
        key={i}
        data={card}
        bodyScrollTop={scrollTop}
        windowDimensions={windowDimensions}
      />
    );
  }

  render() {
    const { cards, windowDimensions } = this.props;
    return (
      <div>
        {cards.isFetching && <div>Fetching data...</div>}
        <div>
          {cards.items.map(this.renderCard)}
        </div>
        <div style={{ margin: `${windowDimensions.canvasSize / 2}px` }}>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // const cards = [
    // { title: 'Title 1', content: 'Canvas Animation 1' },
    // { title: 'Title 2', content: 'Canvas Animation 2' },
    // { title: 'Title 3', content: 'Canvas Animation 3' },
  // ];
  const { cards, windowDimensions } = state;
  return { cards, windowDimensions };
};

export default connect(mapStateToProps, {
  fetchCardsData,
})(HomePage);
