import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { handleGoogleLogin } from '../actions';

const styles = {
  loginPage: {
  },
  loginButton: {
    backgroundColor: '#e06666',
    border: 'none',
    color: 'white',
    padding: '15px 32px',
    textAlign: 'center',
    textDecoration: 'none',
    display: 'inline-block',
    fontSize: '16px',
    cursor: 'pointer',
    alignSelf: 'center',
  },
};

class LoginPage extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleGoogleLogin = this.handleGoogleLogin.bind(this);
  }

  handleGoogleLogin() {
    this.props.handleGoogleLogin();
  }

  render() {
    return (
      <div className="LoginPage"
        style={styles.loginPage}
      >
        <a className="LoginButton"
          style={styles.loginButton}
          onClick={this.handleGoogleLogin}
        >
          Google Login
        </a>
      </div>
    );
  }
}

// LoginPage.propTypes = {
  // handleGoogleLogin: PropTypes.func.isRequired(),
// };

function mapStateToProps(state) {
  const { auth } = state;
  return { auth };
}

export default connect(mapStateToProps, {
  handleGoogleLogin,
})(LoginPage);
