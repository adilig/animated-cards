import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import * as ActionTypes from '../actions';

const windowDimensions = (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.HANDLE_WINDOW_RESIZE:
      return Object.assign({}, action.windowDimensions);
    default:
      return state;
  }
};

const cards = (state = { items: [], isFetching: false }, action) => {
  switch (action.type) {
    case ActionTypes.CARDS_DATA.REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case ActionTypes.CARDS_DATA.SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        items: action.data,
      });
    case ActionTypes.CARDS_DATA.FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });
    default:
      return state;
  }
};

const auth = (state = {}, action) => {
  switch (action.type) {
    case ActionTypes.GOOGLE_LOGIN.SUCCESS:
      return Object.assign({}, state, action.authData);
    case ActionTypes.AUTH_BY_TOKEN.SUCCESS:
      return Object.assign({}, state, action.authData);
    default:
      return state;
  }
};

// Updates error message to notify about the failed fetches.
function errorMessage(state = null, action) {
  const { type, error } = action;

  if (type === ActionTypes.RESET_ERROR_MESSAGE) {
    return null;
  } else if (error) {
    return action.error;
  }

  return state;
}


const rootReducer = combineReducers({
  auth,
  cards,
  windowDimensions,
  routing: routerReducer,
  errorMessage,
});

export default rootReducer;
