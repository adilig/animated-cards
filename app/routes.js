import React from 'react';
import { Route, IndexRoute } from 'react-router';
import authService from './services/auth';
import { handleLogout } from './actions';

import Main from './Main';
import Home from './containers/HomePage';
import Login from './containers/LoginPage';

const Logout = () => <div>Logout</div>;
const Dashboard = () => <div>Dashboard</div>;
const NotFound = () => <div>NotFound</div>;
const isBrowser = typeof(window) !== 'undefined';

const requireAuth = (store) =>
  (nextState, replace) => {
    if (!authService.loggedIn()) {
      if (isBrowser) {
        replace({ pathname: '/login' });
      }
    }
  };

const redirectIfLoggedIn = (store) =>
  (nextState, replace) => {
    if (authService.loggedIn()) {
      replace({ pathname: '/' });
    }
  };

const logout = (store) =>
  (nextState, replace) => {
    store.dispatch(handleLogout());
    replace({ pathname: '/login' });
  };


const getRoutes = (store) => (
  <Route path="/" component={Main}>
    <IndexRoute component={Home} onEnter={requireAuth(store)} />
    <Route path="login" component={Login} onEnter={redirectIfLoggedIn(store)} />
    <Route path="logout" component={Logout} onEnter={logout(store)} />
    <Route path="dashboard" component={Dashboard} onEnter={requireAuth(store)} />
    <Route path="*" component={NotFound} />
  </Route>
);

export default getRoutes;
