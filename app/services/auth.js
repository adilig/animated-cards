const isBrowser = typeof(window) !== 'undefined';

const authService = {
  login(token, cb) {
    if (!isBrowser) { return; }
    if (localStorage.token) {
      if (cb) { cb(true); }
      return;
    }
    localStorage.token = token;
    if (cb) { cb(true); }
  },

  getToken() {
    if (!isBrowser) { return null; }
    return localStorage.token;
  },

  loggedIn() {
    if (!isBrowser) { return null; }
    return !!localStorage.token;
  },

  logout(cb) {
    if (!isBrowser) { return; }
    delete localStorage.token;
    if (cb) cb();
  },
};

export default authService;
