import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../reducers';
import { routerMiddleware } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';

export default function configureStore(history, initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      routerMiddleware(history),
      thunkMiddleware
    )
  );
}
