import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from '../app/store/configureStore';
import { Router, browserHistory } from 'react-router';
import getRoutes from '../app/routes';
import { syncHistoryWithStore } from 'react-router-redux';

require('../app/styl/main.styl');

const store = configureStore(browserHistory, window.__initialState__);
const history = syncHistoryWithStore(browserHistory, store);
const routes = getRoutes(store);

ReactDOM.render(
  <Provider store={store}>
    <Router children={routes} history={history} />
  </Provider>,
  document.getElementById('main')
);
