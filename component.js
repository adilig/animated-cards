import React from 'react';
import ReactDOMServer from'react-dom/server';

class MyComponent extends React.Component {
  render() {
    return <div><h1>Hello World</h1></div>;
  }
}

export default MyComponent;
 
//export default ReactDOMServer.renderToString(<MyComponent />);