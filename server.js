import express from 'express';
import serialize from 'serialize-javascript';
import path from 'path';

import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { createMemoryHistory, match, RouterContext } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import configureStore from './app/store/configureStore';
import getRoutes from './app/routes';

const app = express();
app.use(express.static(path.join(__dirname, 'public')));

if (process.env.NODE_ENV === 'development') {
  const webpack = require('webpack');
  const webpackConfig = require('./webpack.config');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');

  const compiler = webpack(webpackConfig);
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath,
    stats: { colors: true },
  }));
  app.use(webpackHotMiddleware(compiler));
}

const HTML = ({ content, store }) => (
  <html>
    <head>
      <meta charSet="utf-8" />
      <title></title>
      <meta name="viewport" content="initial-scale=1, width=device-width" />
      <link rel="stylesheet" href="style.css" type="text/css" media="all" />
    </head>
    <body>
      <div id="main" dangerouslySetInnerHTML={{ __html: content }} />
      <script
        dangerouslySetInnerHTML={{
          __html: `window.__initialState__=${serialize(store.getState())};`,
        }}
      />
      <script src="bundle.js" type="text/javascript" charSet="utf-8"></script>
    </body>
  </html>
);

function serve(req, res) {
  const memoryHistory = createMemoryHistory(req.url);
  const store = configureStore(memoryHistory);
  const history = syncHistoryWithStore(memoryHistory, store);
  const routes = getRoutes(store);

  const location = req.url;

  match({ history, routes, location }, (error, redirectLocation, renderProps) => {
    if (error) {
      res.status(500).send(error.message);
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      const content = renderToString(
        <Provider store={store}>
          <RouterContext {...renderProps} />
        </Provider>
      );

      const html = renderToString(<HTML content={content} store={store} />);
      res.send(`<!doctype html>\n${html}`);
    }
  });
}

app.get('*', serve);

app.listen(process.env.PORT, process.env.IP);
